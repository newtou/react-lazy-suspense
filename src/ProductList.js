import React from "react";
import "./App.css";

function ProductList({ products }) {
  return (
    <div className="wrapper">
      {products && products.length ? (
        products.map((product) => (
          <div className="card" key={product.id}>
            {product.title}
          </div>
        ))
      ) : (
        <p>No products</p>
      )}
    </div>
  );
}

export default ProductList;
