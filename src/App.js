import React, { lazy, Suspense } from "react";
import logo from "./logo.svg";
import "./App.css";

// lazy load any component by dynamically importing it inside the lazy function
const Product = lazy(() => import("./ProductList"));

function App() {
  const Products = [
    {
      id: 1,
      title: "Mac Book Pro",
    },
    {
      id: 2,
      title: "Dell XPS 15",
    },
    {
      id: 3,
      title: "Alienware M15",
    },
    {
      id: 4,
      title: "ASUS ROG Strix II",
    },
    {
      id: 5,
      title: "Lenovo Legion",
    },
  ];
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>React Lazyloading with (Lazy & Suspense ...)</p>
      </header>

      <div className="product">
        <h3>The Products will appear here</h3>
        <hr />
        {/* Place your fall back jsx or dom object in the fallback 
            prop of suspense. It will be displayed when the products are loading */}
        <Suspense fallback={<div>Loading products ...</div>}>
          <Product products={Products} />
        </Suspense>
      </div>
    </div>
  );
}

export default App;
